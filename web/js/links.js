/**
 * Created by info_000 on 24.10.2016.
 */

var LinksManager = function (options) {
    this.initControls();
}

LinksManager.prototype.initControls = function () {
    var obj = this;
    obj.control = {}

    obj.control.linksTbody   = $('.links-tbody');
    obj.control.linksTable   = $('.links-table');
    obj.control.ajaxLoader   = $(".add-form-loader");
    obj.control.addButton    = $(".add-form-button");
    obj.control.addInput     = $(".add-form-input");

    obj.request = {
        addPath : '/links/create',
        deletePath : '/links/delete-short-link/'
    }

    obj.request.error = function(){
        alert('Произошла ошибка, обратитесь к администратору');
    }

    obj.request.emptyForm = function(){
        alert('Необходимо ввести ссылку');
    }


    obj.helper = {
        cleanForm : function(){
            obj.control.addInput.val('');
        },

        loaderLock : function(){
            obj.control.ajaxLoader.show();
            obj.control.addButton.attr('disabled', 'disabled')
        },

        loaderUnlock : function(){
            obj.control.ajaxLoader.hide();
            obj.control.addButton.removeAttr('disabled');
            obj.helper.cleanForm();
        },

        addItemHtml : function(options){
            obj.control.linksTbody.append(
                '<tr class="short-link-tr" data-item-id="' + options.id + '">' +
                '<td><a href="' + options.url + '" target="_blank">' + options.url + '</a></td>' +
                '<td><a href="/' + options.code + '" target="_blank">' + options.code + '</a></td>' +
                '<td>' + options.date + '</td>' +
                '<td><a title="Удалить короткую ссылку" href="' + obj.request.deletePath + options.id + '" class="cursor-pointer short-link-remove"><span class="glyphicon glyphicon-remove"></span></a></td>' +
                '</tr>'
            )
        },

        removeItemHtml : function(options){
            options.obj.remove();
        }
    }
}

LinksManager.prototype.add = function (options) {
    var obj = this;
    var url = obj.control.addInput.val();

    if (!url){
        obj.request.emptyForm();
        return false;
    }

    obj.helper.loaderLock();

    $.ajax({
        url : obj.request.addPath,
        type : 'POST',
        async : true,
        dataType : 'json',
        data : {
            url : url
        },
        success : function(json){
            if (json.error == false) {
                obj.helper.addItemHtml(json)
            }
            else{
                obj.request.error();
            }

            obj.helper.loaderUnlock();
        },
        error : function(){
            obj.request.error();
            obj.helper.loaderUnlock();
        }
    });
}