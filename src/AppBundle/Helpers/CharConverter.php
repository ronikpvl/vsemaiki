<?php
/**
 * Created by PhpStorm.
 * User: info_000
 * Date: 24.10.2016
 * Time: 16:31
 */

namespace AppBundle\Helpers;


class CharConverter
{
    private static $charSet     = '6AjTIJsNcDHnKRrCGW5XlLfBzb34ivdwYhu0PM9ok7ySpZgOVx8mEa1FqeUtQ2';
    private static $charSetBase = 62;

    private function __construct(){}

    /**
     * @param $id
     * @return bool|string
     */
    public static function encode($id)
    {
        if (!$id) {
            return false;
        }

        $string = self::$charSet[$id % self::$charSetBase];
        while (($id = intval($id / self::$charSetBase)) > 0) {
            $string = self::$charSet[$id % self::$charSetBase] . $string;
        }

        return $string;
    }


    /**
     * @param $code
     * @return bool|int
     */
    public static function decode($code)
    {
        if (!$code) {
            return false;
        }

        $limit    = strlen($code);
        $resultId = strpos(self::$charSet, $code[0]);

        for($i=1; $i<$limit; $i++) {
            $resultId = self::$charSetBase * $resultId + strpos(self::$charSet, $code[$i]);
        }

        return $resultId;
    }
}