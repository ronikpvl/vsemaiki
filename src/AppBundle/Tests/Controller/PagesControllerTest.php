<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PagesControllerTest extends WebTestCase
{
    public function testOne()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/One');
    }

    public function testTwo()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Two');
    }

    public function testThree()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Three');
    }

}
