<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Links;
use AppBundle\Helpers\CharConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class LinksController extends Controller
{
    /**
     * Show all pages
     *
     * @return Response
     */
    public function indexAction()
    {
        $em    = $this->getDoctrine()->getManager();
        $links = $em->getRepository('AppBundle:Links')->findAll();

        return $this->render('AppBundle:Links:index.html.twig', compact('links'));
    }


    /**
     * Create short link
     * Ajax request
     *
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request)
    {
        if (!$request->get('url')) {
            return new Response(json_encode(['error' => true]), '400', ['Content-type' => 'application-json; charset=utf8']);
        }

        $Links = new Links();
        $Links->setUrl($request->get('url'));
        $Links->setCode('');

        $em = $this->getDoctrine()->getManager();
        $em->persist($Links);
        $em->flush();

        $Links->setCode(CharConverter::encode($Links->getId()));
        $em->persist($Links);
        $em->flush();


        return new Response(
            json_encode([
                'id'     => $Links->getId(),
                'url'    => $Links->getUrl(),
                'date'   => $Links->getDate(),
                'code'   => $Links->getCode(),
                'error' => false
            ]),
            '200',
            ['Content-type' => 'application-json; charset=utf8']
        );
    }

    /**
     * Delete short link
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request)
    {
        if (!$request->get('id')){
            return $this->redirectToRoute('links_index');
        }

        $em = $this->getDoctrine()->getManager();
        $links = $em->getRepository('AppBundle:Links')->find($request->get('id'));
        $em->remove($links);
        $em->flush();

        return $this->redirectToRoute('links_index');
    }

    /**
     * Redirect on original page by short link
     * If bad code, redirect on homepage
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectAction(Request $request)
    {
        $em   = $this->getDoctrine()->getManager();
        $code = $request->get('code');

        $itemId = CharConverter::decode($code);

        if (!$itemId){
            return $this->redirectToRoute('app_homepage');
        }

        $links = $em->getRepository('AppBundle:Links')->find($itemId);

        return $this->redirect($links->getUrl());
    }
}
