<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161022180338 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE IF NOT EXISTS `links` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `url` varchar(500) NOT NULL,
              `code` varchar(6) NOT NULL,
              `date` int(11) NOT NULL,
              PRIMARY KEY (`id`),
              KEY `url` (`url`(255)),
              KEY `code` (`code`)
            ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;"
        );

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('DROP TABLE links');
    }
}
